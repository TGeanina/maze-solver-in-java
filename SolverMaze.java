
package mazesolver;

/**
 *
 * @author Geanina Tambaliuc
 */

public class SolverMaze {
    
    public static void SolverMaze(String[][] maze,int x, int y)
    { 
    int[][] visited= new int[maze.length][maze[0].length];
        if(x>=0 && x<maze.length && y>=0 && y<maze[0].length)
        {
            System.out.println(x+","+y);
            visited[x][y]=1;
        while(maze[x][y]!="*")
        {
            if(maze[x][y+1]=="#")
            { if((maze[x-1][y]=="." || maze[x-1][y]=="*")&& visited[x-1][y]!=1)
                {
                    x--;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }
              else if((maze[x][y-1]=="." || maze[x-1][y]=="*")&& visited[x][y-1]!=1)
                {
                    y--;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }
                
                else if((maze[x+1][y]=="." || maze[x+1][y]=="*")&& visited[x+1][y]!=1)
                {
                    x++;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }}
            else if(maze[x-1][y]=="#")
            {  if((maze[x][y-1]=="." || maze[x][y-1]=="*")&& visited[x][y-1]!=1)
                {
                    y--;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }
                else if((maze[x+1][y]=="." || maze[x+1][y]=="*")&& visited[x+1][y]!=1)
                {
                    x++;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }
               
                else if((maze[x][y+1]=="." || maze[x][y+1]=="*")&& visited[x][y+1]!=1)
                {
                    y++;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }}
            else if(maze[x][y-1]=="#")
            { if((maze[x+1][y]=="." || maze[x+1][y]=="*")&& visited[x+1][y]!=1)
                {
                    x++;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }
              else if((maze[x][y+1]=="." || maze[x][y+1]=="*")&& visited[x][y+1]!=1)
                {
                    y++;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }
                 
                
                else if((maze[x-1][y]=="." || maze[x-1][y]=="*")&& visited[x-1][y]!=1)
                {
                    x--;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }}
                else if(maze[x+1][y]=="#") 
                {
                    if((maze[x][y-1]=="." || maze[x][y-1]=="*")&& visited[x][y-1]!=1)
                {
                    y--;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }
                      else if((maze[x][y+1]=="." || maze[x][y+1]=="*")&& visited[x][y+1]!=1)
                {
                    y++;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }
                     else if((maze[x-1][y]=="." || maze[x-1][y]=="*")&& visited[x-1][y]!=1)
                {
                    x--;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }
                    
                }
                else if(maze[x-1][y-1]=="#")
                {
                    if((maze[x][y-1]=="." || maze[x][y-1]=="*")&& visited[x][y-1]!=1)
                {
                    y--;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }
                }
                else if(maze[x-1][y+1]=="#")
                {
                     if((maze[x][y+1]=="." || maze[x][y+1]=="*")&& visited[x][y+1]!=1)
                {
                    y++;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }
                }
                else if(maze[x+1][y-1]=="#")
                {
                     if((maze[x][y-1]=="." || maze[x][y-1]=="*")&& visited[x][y-1]!=1)
                {
                    y--;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }
                }
                else if(maze[x+1][y+1]=="#")
                {
                    
                     if((maze[x][y+1]=="." || maze[x][y+1]=="*")&& visited[x][y+1]!=1)
                {
                    y++;
                    System.out.println(x+","+y);
                    visited[x][y]=1;
                }
                }
                else 
                {System.out.println("Path not found");
                 break;}
                
             
            } 
        
        
            
         
            
        }
        else 
        System.out.println("No Path Found");
        
    }
     public static void main(String[] args)
      {
          String[][] maze1={
              {"#","#","#","#","#","#","#"},
              {"#","*",".",".",".","@","#"},
              {"#","#","#","#","#","#","#"}};
          String [][] maze2={
              {"#","#","#","#","#","#","#"},
              {"#",".",".",".","#","@","#"},
              {"#","*","#",".",".",".","#"},
              {"#","#","#","#","#","#","#"}};
          String[][] maze3={
              {"#","#","#","#","#","#","#","#","#","#","#","#"},
              {"#","#","@",".",".",".",".",".",".",".",".","#"},
              {"#","#","#",".",".","*","#","#","#","#","#","#"},
              {"#","#","#","#","#","#","#","#","#","#","#","#"}};
          String[][] maze4={
              {"#","#","#","#","#","#","#","#","#","#","#","#"},
              {"#",".","#",".",".",".",".",".",".",".",".","#"},
              {"#",".","#",".","#","#","#","#","#","#",".","#"},
              {"#",".","#",".",".",".",".","#",".",".",".","#"},
              {"#",".","#","#","#",".","*","#",".","#",".","#"},
              {"#",".",".",".","#","#","#","#",".","#",".","#"},
              {"#",".","#",".","#",".",".","#",".","#",".","#"},
              {"#",".","#",".","#",".","#","#",".","#",".","#"},
              {"#","@","#",".",".",".",".",".",".","#",".","#"},
              {"#","#","#","#","#","#","#","#","#","#","#","#"}};
          String[][] maze5={
              {"#","#","#","#","#","#","#","#","#","#","#","#"},
              { "#",".","#",".",".",".",".",".",".",".",".","#"},
              {"#",".","#",".","#","#","#","#","#","#",".","#"},
              {"#",".",".",".",".",".",".","#",".",".",".","#"},
              {"#",".","#","#","#",".","@","#",".","#",".","#"},
              {"#",".",".","*","#","#","#","#",".","#",".","#"},
              {"#","#","#","#","#","#","#","#","#","#","#","#"}};
          String[][] maze6={
              {"#","#","#","#","#","#","#","#","#","#"},
              {"#",".",".",".","#",".",".",".",".","#"},
              {"#","*","#",".",".",".",".","#","@","#"},
              {"#",".","#","#","#",".",".","#",".","#"},
              {"#","#","#","#","#","#","#","#","#","#"}};
          System.out.println("Maze 1: ");
          for(int i=0;i<maze1.length;i++)
          {  for(int j=0;j<maze1[0].length;j++)
                  System.out.print(maze1[i][j]+" ");
              System.out.println();
          }
          System.out.println("Path Maze 1: ");
          SolverMaze(maze1,1,5);
          System.out.println();
          System.out.println("Maze 2: ");
          for(int i=0;i<maze2.length;i++)
          {  for(int j=0;j<maze2[0].length;j++)
                  System.out.print(maze2[i][j]+" ");
              System.out.println();
          }
          System.out.println("Path Maze 2: ");
          SolverMaze(maze2,1,5);
          System.out.println();
          System.out.println("Maze 3: ");
          for(int i=0;i<maze3.length;i++)
          {  for(int j=0;j<maze3[0].length;j++)
                  System.out.print(maze3[i][j]+" ");
              System.out.println();
          }
          System.out.println("Path Maze 3: ");
          SolverMaze(maze3,1,2);
          System.out.println();
          System.out.println("Maze 4: ");
          for(int i=0;i<maze4.length;i++)
          {  for(int j=0;j<maze4[0].length;j++)
                  System.out.print(maze4[i][j]+" ");
              System.out.println();
          }
          System.out.println("Path Maze 4: ");
          SolverMaze(maze4,8,1);
          System.out.println();
          System.out.println("Maze 5: ");
          for(int i=0;i<maze5.length;i++)
          {  for(int j=0;j<maze5[0].length;j++)
                  System.out.print(maze5[i][j]+" ");
              System.out.println();
          }
          System.out.println("Path Maze 5: ");
          SolverMaze(maze5,4,6);
          System.out.println();
          System.out.println("Maze 6: ");
          for(int i=0;i<maze6.length;i++)
          {  for(int j=0;j<maze6[0].length;j++)
                  System.out.print(maze6[i][j]+" ");
              System.out.println();
          }
          System.out.println("Path Maze 6: ");
          SolverMaze(maze6,2,8);
          System.out.println();
      }
}
   
