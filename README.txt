How it works?

The program reads a 2D array initiated from 0. Then it finds the starting position marked by "@", and then it tries to reach the final position marked by "*". 
The acceptable steps are marked by "." and the unacceptable steps by "#".

For example:

Maze: 
# # # # # # # # # #
# . . . # . . . . #
# * # . . . . # @ #
# . # # # . . # . #
# # # # # # # # # # 

Right Path:
2,8
1,8
1,7
1,6
1,5
2,5
2,4
2,3
1,3
1,2
1,1
2,1